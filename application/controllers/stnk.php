<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stnk extends CI_Controller{


	public function __construct()
	{
		parent::__construct();
		$this->load->model('liststnk_model');
	}

  public function index(){
    $data = array(
      // '_title'         => 'Barang Masuk',
      // '_subtitle'      => 'Barang Masuk',
      // '_row_dept_use'  => get_dept_use(),
      // '_row_category'  => get_category(),
      // '_row_bugged_cd' => get_bugged_code(),
      // '_row_grade'     => get_grade(),
      // '_headnotif'     => '_headnotif',
      // 'df'             => $this->material_model->get_data_material($id, null, null)->row(),
      // '_stat1'         => $this->statistik_model->get_count_barang_masuk_keluar2(),
      // 'id_material'    => $id,
      '_content'       => 'stnk/index'
    );
    $this->load->view('_head',$data);
    $this->load->view('_body',$data);
    $this->load->view('_footer');
  }
	public function add_urusan_stnk(){
    $data = array(
      // '_title'         => 'Barang Masuk',
      // '_subtitle'      => 'Barang Masuk',
      // '_row_dept_use'  => get_dept_use(),
      // '_row_category'  => get_category(),
      // '_row_bugged_cd' => get_bugged_code(),
      // '_row_grade'     => get_grade(),
      // '_headnotif'     => '_headnotif',
      // 'df'             => $this->material_model->get_data_material($id, null, null)->row(),
      // '_stat1'         => $this->statistik_model->get_count_barang_masuk_keluar2(),
      // 'id_material'    => $id,
      '_content'       => 'stnk/form'
    );
    $this->load->view('_head',$data);
    $this->load->view('_body',$data);
    $this->load->view('_footer',$data);
  }

  public function add_new_stnk()
  {
    $data = array(
      // '_title'         => 'Barang Masuk',
      // '_subtitle'      => 'Barang Masuk',
      // '_row_dept_use'  => get_dept_use(),
      // '_row_category'  => get_category(),
      // '_row_bugged_cd' => get_bugged_code(),
      // '_row_grade'     => get_grade(),
      // '_headnotif'     => '_headnotif',
      // 'df'             => $this->material_model->get_data_material($id, null, null)->row(),
      // '_stat1'         => $this->statistik_model->get_count_barang_masuk_keluar2(),
      // 'id_material'    => $id,
      '_content'       => 'stnk/form_stnk'
    );
    $this->load->view('_head',$data);
    $this->load->view('_body',$data);
    $this->load->view('_footer');
  }

  public function new_stnk(){
    $this->login_users->keamanan();
    // $data = array(
    //   '_title'         => 'Barang Masuk',
    //   '_subtitle'      => 'Barang Masuk',
    //   '_row_dept_use'  => get_dept_use(),
    //   '_row_category'  => get_category(),
    //   '_row_bugged_cd' => get_bugged_code(),
    //   '_row_grade'     => get_grade(),
    //   '_headnotif'     => '_headnotif',
    //   'df'             => $this->material_model->get_data_material($id, null, null)->row(),
    //   '_stat1'         => $this->statistik_model->get_count_barang_masuk_keluar2(),
    //   'id_material'    => $id,
    //   '_content'       => 'barang_masuk/_frm_barang_masuk'
    // );

    // $this->load->view('_head',$data);
    $this->load->view('_head');
    $this->load->view('_body');
    $this->load->view('_footer');
  }

  public function ajax_list()
	{
		$list = $this->liststnk_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $liststnk) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $liststnk->no_plat;
			$row[] = $liststnk->nama_pemilik;
			$row[] = $liststnk->alamat;
			$row[] = $liststnk->merk;
			$row[] = $liststnk->type;
			$row[] = $liststnk->jenis;
			$row[] = $liststnk->model;
			$row[] = $liststnk->tahun_pembuatan;
			$row[] = $liststnk->isi_silinder;
			$row[] = $liststnk->no_rangka;
			$row[] = $liststnk->no_mesin;
			$row[] = $liststnk->warna;
			$row[] = $liststnk->bahan_bakar;
			$row[] = $liststnk->warna_plat;
			$row[] = $liststnk->tahun_registrasi;
			$row[] = $liststnk->no_bpkb;
			$row[] = $liststnk->no_lokasi;
			$row[] = $liststnk->no_urut_daftar;
			$row[] = $liststnk->masa_berlaku;
			$row[] = $liststnk->status_stnk; //set column field database for datatable searchable


			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->liststnk_model->count_all(),
						"recordsFiltered" => $this->liststnk_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

}
