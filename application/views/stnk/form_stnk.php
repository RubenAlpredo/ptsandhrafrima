<!-- no_plat
nama_pemilik
alamat
merk
type
jenis
model
tahun_pembuatan
isi_silinder
no_rangka
no_mesin
warna
bahan_bakar
warna_plat
tahun_registrasi
no_bpkb
no_lokasi
no_urut_daftar
masa_berlaku
status_stnk
create_date
update_date -->

  <!-- <div class="column is-12">
    <label class="label" for="email">Email</label>
    <p :class="{ 'control': true }">
        <input v-validate="'required|email'" :class="{'input': true, 'is-danger': errors.has('email') }" name="email" type="text" placeholder="Email">
        <span v-show="errors.has('email')" class="help is-danger">{{ errors.first('email') }}</span>
    </p>
  </div> -->


<!-- <form class="" action="index.html" method="post" > -->
<div class="" id="app">
  <div class="row">
    <div class="form-group mrg-top">
      <label class="col-md-2 col-md-offset-1 control-label" for="no_plat">Nomor Plat</label>
      <div >
        <div  class="col-md-6" v-bind:class="{ 'control': true }">
          <input class="form-control input-md" v-validate="'required'" v-bind:class="{'input': true, 'is-danger': errors.has('no_plat') }" name="no_plat" type="text" placeholder="Email">
          <span v-show="errors.has('no_plat')" class="help is-danger">{{ errors.first('no_plat') }}</span>
        </div>
      <!-- <input id="no_plat" name="no_plat" placeholder="Nomor Plat" class="form-control input-md" type="text"> -->


      </div>
    </div>
  </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="nama_pemilik">Nama Pemilik</label>
      <div class="col-md-6">
        <input id="nama_pemilik" name="nama_pemilik" placeholder="Nama Pemilik" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="alamat">Alamat</label>
      <div class="col-md-6">
        <textarea id="alamat" name="alamat" placeholder="Alamat" class="form-control input-md"  rows="8" cols="80"></textarea>
      </div>
    </div>
  </div>
  <div class="row">
    <!-- Helper Merk -->
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="merk">merk</label>
      <div class="col-md-6">
        <input id="merk" name="merk" placeholder="Merk" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="type">Type</label>
      <div class="col-md-6">
        <input id="type" name="type" placeholder="Type" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="jenis">Jenis</label>
      <div class="col-md-6">
        <input id="jenis" name="jenis" placeholder="Jenis" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="model">Model</label>
      <div class="col-md-6">
        <input id="jenis" name="model" placeholder="model" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="tahun_pembuatan">Tahun Pembuatan</label>
      <div class="col-md-6">
        <input id="tahun_pembuatan" name="tahun_pembuatan" placeholder="Tahun Pembuatan" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="isi_silinder">Isi Silindeer</label>
      <div class="col-md-6">
        <input id="isi_silinder" name="isi_silinder" placeholder="isi_silinder" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="no_rangka">No Rangka</label>
      <div class="col-md-6">
        <input id="no_rangka" name="no_rangka" placeholder="no_rangka" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="no_mesin">No Mesin</label>
      <div class="col-md-6">
        <input id="no_mesin" name="no_mesin" placeholder="no_mesin" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="warna">Warna</label>
      <div class="col-md-6">
        <input id="warna" name="warna" placeholder="warna" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="bahan_bakar">Bahan Bakar</label>
      <div class="col-md-6">
        <input id="bahan_bakar" name="bahan_bakar" placeholder="Bahan Bakar" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="warna_plat">Warna Plat</label>
      <div class="col-md-6">
        <input id="warna_plat" name="warna_plat" placeholder="Warna Plat" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="tahun_registrasi">Tahun Registrasi</label>
      <div class="col-md-6">
        <input id="tahun_registrasi" name="tahun_registrasi" placeholder="Tahun Registrasi" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="no_bpkb">No Bpkb</label>
      <div class="col-md-6">
        <input id="no_bpkb" name="no_bpkb" placeholder="No Bpkb" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="no_lokasi">No Lokasi</label>
      <div class="col-md-6">
        <input id="no_lokasi" name="no_lokasi" placeholder="No Lokasi" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="no_urut_daftar">No Urut Daftar</label>
      <div class="col-md-6">
        <input id="no_urut_daftar" name="no_urut_daftar" placeholder="No Urut Daftar" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="no_urut_daftar">Masa Berlaku</label>
      <div class="col-md-6">
        <input id="masa_berlaku" name="masa_berlaku" placeholder="Masa Berlaku" class="form-control input-md" type="text">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="form-group mrg-top">
      <label  class="col-md-2 col-md-offset-1 control-label" for="Nom22"></label>
      <button class="col-md-2 btn btn-primary btn" style="margin-left:15px;">Simpan</button>
    </div>
  </div>
</form>
<script src="<?php echo site_url('vue/vue.js'); ?>" type="text/javascript"></script>
<script src="<?php echo site_url('vue/vee-validate.js'); ?>" type="text/javascript"></script>
<script>
    Vue.use(VeeValidate); // good to go.
</script>
<script src="<?php echo site_url('vue/app.js'); ?>" type="text/javascript"></script>
