<div class="custom-tabs-line tabs-line-bottom left-aligned">
  <ul class="nav" role="tablist">
  <li class="active"><a href="#tab-bottom-left1" role="tab" data-toggle="tab" aria-expanded="false">STNK</a></li>
  <li class=""><a href="#tab-bottom-left2" role="tab" data-toggle="tab" aria-expanded="true">Urusan STNK </a></li>
  <li class=""><a href="#tab-bottom-left3" role="tab" data-toggle="tab" aria-expanded="true">Urusan Pajak </a></li>
  </ul>
</div>
<div class="tab-content">
  <div class="tab-pane fade in active" id="tab-bottom-left1">
    <div class="row">
      <div class="col-md-12">
          <table id="table" class="display table-bordered table-striped" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>No</th>
                      <th>no_plat</th>
                      <th>nama_pemilik</th>
                      <th>alamat</th>
                      <th>merk</th>
                      <th>type</th>
                      <th>jenis</th>
                      <th>model</th>
                      <th>tahun_pembuatan</th>
                      <th>isi_silinder</th>
                      <th>no_rangka</th>
                      <th>no_mesin</th>
                      <th>warna</th>
                      <th>bahan_bakar</th>
                  </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
      </div>
    </div>
  </div>
  <div class="tab-pane fade" id="tab-bottom-left2">
    <div class="row">
      <div class="col-md-12">

      </div>
    </div>
  </div>
</div>
