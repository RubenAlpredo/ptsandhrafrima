<!doctype html>
<html lang="en">

<head>
	<title>Dashboard | Klorofil - Free Bootstrap Dashboard Template</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="<?php echo site_url('assets/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo site_url('assets/css/vendor/icon-sets.css');?>">
	<link rel="stylesheet" href="<?php echo site_url('assets/css/main.css');?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?php echo site_url('assets/css/demo.css');?>">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url('assets/img/apple-icon.png');?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url('assets/img/favicon.png');?>">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<vsidebar></vsidebar>
		<!-- END SIDEBAR -->


		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<vnavbar></vnavbar>
			<pemilikbarunav></pemilikbarunav>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content" style="margin:-8px !important;">
				<div class="container-fluid">
					<!--
					<div class="custom-tabs-line tabs-line-bottom left-aligned">
						<ul class="nav" role="tablist">
						<li class=""><a href="#tab-bottom-left1" role="tab" data-toggle="tab" aria-expanded="false">Recent Activity</a></li>
						<li class="active"><a href="#tab-bottom-left2" role="tab" data-toggle="tab" aria-expanded="true">Projects <span class="badge">7</span></a></li>
						</ul>
					</div> -->
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<!-- <test></test> -->
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="form-group mrg-top">
								  <label class="col-md-2 col-md-offset-1 control-label" for="Nom22">No Ktp</label>
								  <div class="col-md-6">
								  <input id="Nom22" name="Nom22" placeholder="No Ktp" class="form-control input-md" required="" type="text">
								  </div>
								</div>
							</div>
							<div class="row">
								<div class="form-group mrg-top">
									<label  class="col-md-2 col-md-offset-1 control-label" for="Nom22">Nama Pemilik</label>
									<div class="col-md-6">
										<input id="Nom22" name="Nom22" placeholder="Nama Pemilik" class="form-control input-md" required="" type="text">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group mrg-top">
									<label  class="col-md-2 col-md-offset-1 control-label" for="Nom22">No Plat</label>
									<div class="col-md-6">
										<input id="Nom22" name="Nom22" placeholder="No Plat" class="form-control input-md" required="" type="text">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group mrg-top">
									<label  class="col-md-2 col-md-offset-1 control-label" for="Nom22">No Hp/Telp</label>
									<div class="col-md-6">
										<input id="Nom22" name="Nom22" placeholder="No Hp/Telp" class="form-control input-md" required="" type="text">

									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group mrg-top">
									<label  class="col-md-2 col-md-offset-1 control-label" for="Nom22">Thn Masuk</label>
									<div class="col-md-6">
										<input id="Nom22" name="Nom22" placeholder="Thn Masuk" class="form-control input-md" required="" type="text">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group mrg-top">
										<label  class="col-md-2 col-md-offset-1 control-label" for="Nom22">Alamat</label>

											<div class="col-md-6">
												<textarea class="form-control" placeholder="textarea" rows="4"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group mrg-top">
									<label  class="col-md-2 col-md-offset-1 control-label" for="Nom22">Keterangan</label>
									<div class="col-md-6">
										<textarea class="form-control" placeholder="textarea" rows="4"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group mrg-top">
									<label  class="col-md-2 col-md-offset-1 control-label" for="Nom22">Status</label>
									<div class="col-md-6">
									<input id="Nom22" name="Nom22" placeholder="Status" class="form-control input-md" required="" type="text">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group mrg-top">
									<label  class="col-md-2 col-md-offset-1 control-label" for="Nom22"></label>
									<button class="col-md-2 btn btn-primary btn" style="margin-left:15px;">Simpan</button>
								</div>
							</div>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; 2016</p>
				</div>
			</footer>
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<?php echo site_url('assets/js/jquery/jquery-2.1.0.min.js'); ?>"></script>
	<script src="<?php echo site_url('assets/js/bootstrap/bootstrap.min.js');?>"></script>
	<script src="<?php echo site_url('assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js');?>"></script>
	<script src="<?php echo site_url('assets/js/klorofil.min.js');?>"></script>
	<script src="<?php echo site_url('vue/vue.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo site_url('vue/vue-router.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo site_url('vue/axios.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo site_url('vue/vee-validate.js'); ?>" type="text/javascript"></script>
	<script>
			Vue.use(VeeValidate); // good to go.
	</script>
	<script src="<?php echo site_url('vue/app.js'); ?>" type="text/javascript"></script>

</body>

</html>
