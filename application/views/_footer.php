<!-- END WRAPPER -->
<!-- Javascript -->

<script src="<?php echo site_url('vue/vue-router.js'); ?>" type="text/javascript"></script>
<script src="<?php echo site_url('vue/axios.js'); ?>" type="text/javascript"></script>
<script src="<?php echo site_url('assets/js/jquery/jquery-2.2.3.min.js'); ?>"></script>
<script src="<?php echo site_url('assets/js/bootstrap/bootstrap.min.js');?>"></script>
<script src="<?php echo site_url('assets/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php //echo site_url('assets/js/dataTables.bootstrap.min.js')?>"></script>
<script src="<?php echo site_url('assets/js/dataTables.responsive.min.js')?>"></script>
<script src="<?php //echo site_url('assets/js/dataTables.buttons.min.js')?>"></script>
<script src="<?php //echo site_url('assets/js/buttons.bootstrap.min.js')?>"></script>
<script src="<?php echo site_url('assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo site_url('assets/js/klorofil.min.js');?>"></script>




<script type="text/javascript">

var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({
        responsive: true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('stnk/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],

    });

});

</script>


</body>

</html>
